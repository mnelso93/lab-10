﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'
// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/SpecularShader" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_SpecColour("Specular Color", Color) = (1,1,1,1)
		_Shininess("Shininess", float) = 10
	}
	SubShader {
	Pass{
		
		
		CGPROGRAM
		#pragma vertex vertexFunction
		#pragma fragment fragmentFunction

		//user defined variables
		uniform float4 _Color;
		uniform float4 _SpecColour;
		uniform float _Shininess;

		//unity defined variables
		uniform float4 _LightColor0;

		//input struct
		struct inputStruct 
		{
			float4 vertexPos : POSITION;
			float3 vertexNormal : NORMAL;
		};

		//output struct
		struct outputStruct
		{
			float4 pixelPos: SV_POSITION;
			

			float3 normalDirection : TEXCOORD0;
			float4 pixelWorldPos : TEXCOORD1;
		};

		//vertex program
		outputStruct vertexFunction(inputStruct input)
		{
			outputStruct toReturn;
			

			toReturn.pixelWorldPos = mul(unity_ObjectToWorld, input.vertexPos);
			toReturn.normalDirection = normalize(mul(float4(input.vertexNormal, 0.0), unity_WorldToObject).xyz);
			toReturn.pixelPos = UnityObjectToClipPos(input.vertexPos);
			return toReturn;
		}

		//fragment program
		float4 fragmentFunction(outputStruct input) : COLOR
		{
			float3 viewDirection = normalize(float3(float4(_WorldSpaceCameraPos.xyz, 1.0) - input.pixelWorldPos.xyz));


			float3 lightDirection;
			float attenuation = 1.0;
			lightDirection = normalize(-_WorldSpaceLightPos0.xyz);
			float3 diffuseReflection = attenuation * _LightColor0.xyz * max(0.0, dot(input.normalDirection, lightDirection));

			float3 specularReflection = reflect(-lightDirection, input.normalDirection);
			specularReflection = dot(specularReflection, viewDirection);
			specularReflection = pow(max(0.0, specularReflection), _Shininess)  * _SpecColour;
			specularReflection = max(0.0, dot(input.normalDirection, lightDirection)) * specularReflection;

			float3 finalLight = specularReflection + diffuseReflection + UNITY_LIGHTMODEL_AMBIENT;
			return float4(finalLight, 1.0) * _Color;
		}
		ENDCG
		}
	}

	//FallBack "Diffuse"
}
